



## Deductive Verification 

The verification is performed on CL code translated from Rust code.
It consists of three steps:
1. Run Rust semantics to get CL code
2. Specify properites
3. Run K framework deductive verification

All benchmarks for deductive verification can be found at the folder verification
Here we show a small example, which is easy to understand:

```
fn sum(a:i32, b:i32) -> i32 {
   a + b
}
```

The function computes the sum of a and b.

If we want run the program, we need to add main function as:

```
fn main(){
   sum(3+4);
}
```

If we don't add the main function, run Rust level semantics by :

      krun sum.k

we can get
the following result (full result see [full result](sum))

```
   ... ...
   
   <tstack>
       ListItem ( fun sum ( var ( 0 ) , var ( 1 ) , .Idents ) { (| #moveVal ( var ( 0 ) +cl var ( 1 ) , var ( 0 ) +cl var ( 1 ) ) |) } )
   </tstack>

   ... ...

```

The cell ''tstack'' shows corresponding CL code of the function sum.

 * var(0) is a variable in CL corresponds to the paramter a, and
 
 * var(1) is a variable in CL corresponds to the parameter b

The specification is the following:

   **precondition**
   
   ```
   
         - x is a variable of an integer
	 
         - y is a variaboe of an integer
	 
   ```

   **code**
   
   ```
   
	call the function sum with x and y
	
   ```

   **postcondition**

   ```
   
	- sum(x,y) == x + y
	
   ```
   
The propertiy specification in can be found [here](sum-spec.k)

run the command line:

  bin/clkprove sum-spec.k

The verification result is

``` 
     #True
```

If we change the postcondition as ``` sum(x,y) == x - y ```

The verification result will give a counter example,

which shows the program does not satisify

the property.

The verification Result of the postcondition ``` sum(x,y) == x - y ```
is as follows:

```
   ... ...


    <env>
        re |-> valcl ( V0 +Int V1 )
        x |-> valcl ( V0 )
        y |-> valcl ( V1 )
      </env>


   ... ...
```

It means that re equals x + y instead of x - y.

The full counter example can be found [here](sum_result.md)



















/////////////////////////////////////////////////////////////////////////////////





















