
struct RawVec < T > {
       ptr: * mut T,
       Cap: usize
}

impl RawVec <T> {

     fn new() -> RawVec<T> {
       RawVec {
         ptr: #rallocate(#TyUndef,0),
         Cap: 0
       }
     }

     fn with_capacity(cap: usize) -> RawVec<T> {
        RawVec {
            ptr: #rallocate(T, cap)
        }
     }

     fn cap(& self) -> usize {
        (*self).Cap
     }

     fn from_raw_parts(ptr1: * mut T, capacity: usize) -> RawVec< T > {
        RawVec {
               ptr : ptr1,
               Cap : capacity
        }
     }

}


struct Vec < T > {
    buf: RawVec <T>,
    Len : usize
}


impl Vec < T > {

     fn len(& self) -> usize {
        (*self). Len
     }

     fn new() -> Vec<T> {
        Vec {
            buf: RawVec::new(),
            Len: 0
        }
     }
 fn with_capacity(capacity: usize) -> Vec < T > {
        Vec {
            buf: RawVec::with_capacity(capacity),
            Len: 0
        }
     }

     fn capacity(& self) -> usize {
        ((& ((*self).buf)).cap)()
     }

     fn from_raw_parts(ptr1: * mut T, length: usize, capacity: usize) -> Vec < T > {
        Vec {
            buf: RawVec::from_raw_parts(ptr1, capacity),
            Len: length
	}
     }


}


fn insert(v: &mut Vec<i32>, pos: usize, value: i32){
        let mut pos = pos - 1;
        while (pos >= 0) {
            if (*v)[pos] <= value {
                break;
            };
            (*v)[pos + 1] = ((*v)[pos]);
            pos = pos - 1;
        }
        (*v)[pos + 1] = value;
    }

fn insertion_sort(v: &mut Vec< i32 >) {

    let len = v.len();
    let mut i = 1;
    while(i < len){
    	    let value = (*v)[i];  
	    insert(v, i , value);
	    i = i + 1;
    }
}

fn Vec_Order(v: & Vec< i32 > ) -> bool {
   let len = v.len();
   let mut i = 1;
   while(i < len) {
   	   if ( ((*v)[i - 1] )> ((* v) [i])) {
	      return false;
	   };
	   i = i + 1;
   }
   return true;
}

fn main() {
    let mut v = vec![4,5,6,7,1,2,3];
    insertion_sort(& mut v);
    assert!(Vec_Order(& v) == true);

}


