


### Compile and Run the Semantics

When run a Rust program using Rust level semantics, Rust level semantics 
automatically performs the following steps:
  - step 1 : translate a Rust program to a Core Langage (CL) program
  - step 2 : execute the CL program with CL level semantics


Even though Rust level semantics refers to CL level semantics, CL level
semantics is indepdent of Rust level semantics.

If we just want to run CL semantics. The following step should be done:

 - enter the folder clang_op
  - open the file: clang_op/importcl.k and modify the line : require "../configuration.k" to require "configuration.k", and also the typedef.k

  - run  the command 

          kompile clang.k --backend ocaml



#### Run rust programs with Rust level semantics


In order to a Rust program, run the following command line:

         krun "the path to the Rust program file"

For example, when we want to run the program in "demo/demo1.rs", we run the command
   
          krun demo/demo1.rs 

The content of demo1.rs is:

```

struct Point {
       x:i32,
       y:i32
}

fn main() {

   let p = Point{x:2,y:3};
   let t = p.x + p.y;

   println!("{}", t);
}

```

The following the result after krun the code (The comments explain the result):

```

<T>
  <threads>
    <thread>	       //only one thread is executed
      <threadId>
        0
      </threadId>
      <tname>
        "main"
      </tname>
      <k>
        #unit		//#unit means all code are executed
      </k>
      <env>		//environment is empty
        .Map
      </env>
      <clstack>		//stack is empty
        .List
      </clstack>
    </thread>
    <cntThreads>
      1
    </cntThreads>
  </threads>
  <TCContext>
    <varCtx>
      <varCnt>
        2
      </varCnt>
      <vInfo>
        var ( 0 ) |-> varInfo ( p , own ( #prodId ( Point ) ) , 1 )	//This is the information of variable p
        var ( 1 ) |-> varInfo ( t , i32 , 1 )	    	      	  	//This is the information of variable t
      </vInfo>
    </varCtx>
    <typeCtx>
      #prodId ( Point ) |-> 0						//the type point is stored at the index 0 of the cell comtypes
      main |-> fnTy ( .RSTypes ; void )
    </typeCtx>
    <cpable>
      SetItem ( bool )
      SetItem ( i32 )
    </cpable>
    <tcenv>
      .Map
    </tcenv>
    <stackEnv>
      .List
    </stackEnv>
    <lifetime>
      <currentLft>
        0
      </currentLft>
    </lifetime>
    <comtypes>
      <comtype>
        <ctyId>		//The type of Point is stored here
          0
        </ctyId>
        <ctyKind>
          0
        </ctyKind>
        <ctyElem>
          0 |-> i32
          1 |-> i32
        </ctyElem>
        <cntElem>
          1
        </cntElem>
      </comtype>
      <ctyCnt>
        1
      </ctyCnt>
    </comtypes>
    <desugar>			//the CL code translated from Rust is shown in the following cell "tstack"
      <tstack>		
        ListItem ( fun main ( .Idents ) { (| letcl var ( 0 ) =cl allocateInit ( 0 |-> #moveVal ( valcl ( 2 ) , valcl ( 2 ) )
        1 |-> #moveVal ( valcl ( 3 ) , valcl ( 3 ) ) , #prodId ( Point ) ) incl ( #move ( allocateInit ( 0 |-> #moveVal ( valcl ( 2 ) , valcl ( 2 ) )
        1 |-> #moveVal ( valcl ( 3 ) , valcl ( 3 ) ) , #prodId ( Point ) ) ) ;cl letcl var ( 1 ) =cl #cvarcl ( var ( 0 ) .cl valcl ( 0 ) ) +cl #cvarcl ( var ( 0 ) .cl valcl ( 1 ) ) incl #print ( "{}" , var ( 1 ) , .ExpCLs ) ) |) } )
      </tstack>
      <closureId>
        0
      </closureId>
      <id2type>
        Point |-> #prodId ( Point )
        str |-> str
      </id2type>
      <f2type>
        Point . x |-> 0
        Point . y |-> 1
      </f2type>
    </desugar>
  </TCContext>
  <OPContext>
    <closures>			//The following are all closres. In fact, there is only one closure, for the function main
      <closureCnt>
        1
      </closureCnt>
      <funclosure>
        main |-> 0
      </funclosure>
      <closure>
        <crId>
          0
        </crId>
        <crContext>
          .
        </crContext>
        <crParams>
          .Idents
        </crParams>
        <crBody>
          (| letcl var ( 0 ) =cl allocateInit ( 0 |-> #moveVal ( valcl ( 2 ) , valcl ( 2 ) )
          1 |-> #moveVal ( valcl ( 3 ) , valcl ( 3 ) ) , #prodId ( Point ) ) incl ( #move ( allocateInit ( 0 |-> #moveVal ( valcl ( 2 ) , valcl ( 2 ) )
          1 |-> #moveVal ( valcl ( 3 ) , valcl ( 3 ) ) , #prodId ( Point ) ) ) ;cl letcl var ( 1 ) =cl #cvarcl ( var ( 0 ) .cl valcl ( 0 ) ) +cl #cvarcl ( var ( 0 ) .cl valcl ( 1 ) ) incl #print ( "{}" , var ( 1 ) , .ExpCLs ) ) |)
        </crBody>
      </closure>
    </closures>
    <memory>
      <memaddress>
        1
      </memaddress>
      <blocks>			//The memory blocks
        <block>
          <baddress>
            #freeA ( 0 )	//#freeA(0) means that the block of the address 0 is freed
          </baddress>
          <bnum>
            2
          </bnum>
          <bstore>
            0 |-> valcl ( 2 )	//the block stores the value of the type Point, where first one is 2 and the second one 3
            1 |-> valcl ( 3 )
          </bstore>
          <btype>
            #prodId ( Point )
          </btype>
        </block>
      </blocks>
      <memorystatus>
        0 |-> #freeT ( memstatus ( 0 , 0 ) )
      </memorystatus>
      <cntmem>
        .Map
      </cntmem>
    </memory>
    <tmpk>
      .
    </tmpk>		
    <output>		      //Print the value of t. The output cell is output stream, here "5" is inserted
      ListItem ( "5" )
    </output>
  </OPContext>
  <veriIds>
    "syntax Id ::="
  </veriIds>
</T>


```


#### Run CL programs with CL level semantics

We have written a shell script to run the CL semantics in "bin/clkrun".

When run a CL program, run the following command line:

     	 bin/clkrun "the path the CL proram"


For example, when we want to run a program in cl_tests/function/t1.rs,
just run

         bin/clkrun cl_tests/function/t1.rs


































